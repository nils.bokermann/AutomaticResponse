About
=====

AutomaticResponse is a small AppleScript to create reply messages. I wrote it to
get a simple way to send refusals to project offer from project facilitators.

 

Configuration
-------------

You must carefully edit the properties at the beginning of the script. Some
fixed Strings as the Salutation and the next available date will be configured
in the first section.

In the second section you will find configuration items for the Mail Account and
the mailboxes used.
