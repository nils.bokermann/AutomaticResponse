(*
BEER-WARE LICENSE
Nils Bokermann <nils.bokermann+github@bermuda.de> wrote this Software. As long as you retain
this notice you can do whatever you want with this stuff. If we meet some day,
and you think this stuff is worth it, you can buy me a beer in return.
Nils Bokermann
*)

-- Fixed Strings
property derecognizedTill : "Dezember 2018"
property salutationMale : "Sehr geehrter Herr "
property salutationFemale : "Sehr geehrte Frau "

-- Mail Account Settings
property mailbox_m: "AbgelehnteProjekte_m"
property mailbox_f: "AbgelehnteProjekte_f"
property mailaccount: "Bermuda"

using terms from application "Mail"
	on perform mail action with messages {messageList, choice}
		copy messageList to messagesToAnswer
		repeat with toAnswer in messagesToAnswer
			tell sender of toAnswer
				set thefromname to (extract name from it)
				set thefromaddress to (extract address from it)
			end tell
			log thefromname
			log thefromaddress
			if choice is "f" then
				set female to true
			else
				set female to false
			end if
			log female
			if female is true then
				set messagecontent to salutationFemale & thefromname & ","
			else
				set messagecontent to salutationMale & thefromname & ","
			end if
			set messagecontent to messagecontent & return & return
			set messagecontent to messagecontent & "Vielen Dank für das interessante Projektangebot. Leider bin ich bis vorraussichtlich " & derecognizedTill & " ausgebucht."
			set messagecontent to messagecontent & return & return
			set messagecontent to messagecontent & "Viele Grüße," & return & tab & "Nils Bokermann"
			set messagecontent to messagecontent & return & return
			set messagecontent to messagecontent & "P.S.: Bitte beachten Sie auch die Hinweise für Projektvermittler auf meine Web-Seite: http://www.bermuda.de/vermittler"
			set messagecontent to messagecontent & return
			set messagecontent to messagecontent & "Vielen Dank."

			tell application "Finder"
				set the clipboard to messagecontent
			end tell
			set answer to reply toAnswer #  without opening window
			delay 0.1
			tell application "Mail" to activate
			tell application "System Events" to keystroke "v" using {command down}
			delay 0.1
			tell application "System Events" to keystroke "d" using {command down, shift down}
			move toAnswer to mailbox "Projekte" of account "Bermuda"
		end repeat
	end perform mail action with messages
end using terms from

using terms from application "Mail"
	on run
		tell application "Mail" to set mySelection to messages of mailbox mailbox_m of account mailaccount
		log (length of mySelection)
		tell me to perform mail action with messages {mySelection, "m"}
		tell application "Mail" to set mySelection to messages of mailbox mailbox_f of account mailaccount
		log (length of mySelection)
		tell me to perform mail action with messages {mySelection, "f"}
	end run
end using terms from
